package isi.CoupeSpring.Entities;

import java.io.Serializable;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;

@Entity
public class Arbitre implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column(name="arbitreid")
	private Long id;
	private String nom;
	private int cin;
	private int age;
	private String nationnalite;
	
	@OneToMany(mappedBy="arbitres",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	private Set<Jeu> jeux;
	


	
	public Arbitre() {
		super();
	}


	public Arbitre(Long id, String nom, int cin, int age, String nationnalite, Set<Jeu> jeux) {
		super();
		this.id = id;
		this.nom = nom;
		this.cin = cin;
		this.age = age;
		this.nationnalite = nationnalite;
		this.jeux = jeux;
	}


	@Override
	public String toString() {
		return "Arbitre [id=" + id + ", nom=" + nom + ", cin=" + cin + ", age=" + age + ", nationnalite=" + nationnalite
				+ ", jeux=" + jeux + "]";
	}


	public Set<Jeu> getjeux() {
		return jeux;
	}


	public void setjeux(Set<Jeu> jeux) {
		this.jeux = jeux;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public int getCin() {
		return cin;
	}


	public void setCin(int cin) {
		this.cin = cin;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}


	public String getNationnalite() {
		return nationnalite;
	}


	public void setNationnalite(String nationnalite) {
		this.nationnalite = nationnalite;
	}


	

	
	
	
}

