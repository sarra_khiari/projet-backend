package isi.CoupeSpring.Entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity

public class Produit  implements Serializable{
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String destignation ;
	private double price;
	private int quantite;
	
	public Produit(Long id, String destignation, double price, int quantite) {
		super();
		this.id = id;
		this.destignation = destignation;
		this.price = price;
		this.quantite = quantite;
	}
	public Produit() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDestignation() {
		return destignation;
	}
	public void setDestignation(String destignation) {
		this.destignation = destignation;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	@Override
	public String toString() {
		return "Produit [id=" + id + ", destignation=" + destignation + ", price=" + price + ", quantite=" + quantite
				+ "]";
	}
	
}
