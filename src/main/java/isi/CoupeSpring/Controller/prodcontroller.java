package isi.CoupeSpring.Controller;

import java.util.List;




import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import isi.CoupeSpring.Entities.*;
import isi.CoupeSpring.Repository.*;


@RestController

public class prodcontroller {
	@Autowired 
	private ProduitRepository po;
	
	@GetMapping(value="/listproduits")
	public List<Produit> listproduits() {
		return po.findAll();
	}
	@GetMapping(value="/listproduits/{id}")
	public Produit listproduits(@PathVariable(name="id") Long id) {
		return po.findById(id).get();	}
	
	@PostMapping(value="/listproduits")
	public Produit save( @RequestBody Produit a) {
	    return po.save(a);
	}
	@PutMapping(value="/listproduits/{id}")
	public Produit update(@PathVariable(name="id") Long id,@RequestBody Produit a) {
	   a.setId(id);
		return po.save(a);
	}
	@DeleteMapping(value="/listproduits/{id}")
	public void  delete(@PathVariable(name="id") Long id) {
	    po.deleteById(id);
	}
	
	
}
