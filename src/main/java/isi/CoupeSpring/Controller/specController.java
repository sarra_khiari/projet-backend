package isi.CoupeSpring.Controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import isi.CoupeSpring.Entities.*;
import isi.CoupeSpring.Repository.*;


@RestController

public class specController {
	@Autowired 
	private specRepo po;
	
	@GetMapping("/listspectateurs")
	public List<Spectateur> ListSpectaeurs() {
		return po.findAll();
	}
	@GetMapping(value="/listspectateurs/{id}")
	public Spectateur ListSpectaeurs(@PathVariable(name="id") Long id) {
		return po.findById(id).get();	}
	
	@PostMapping(value="/listspectateurs")
	public Spectateur save( @RequestBody Spectateur a) {
	    return po.save(a);
	}
	@PutMapping(value="/listspectateurs/{id}")
	public Spectateur update(@PathVariable(name="id") Long id,@RequestBody Spectateur a) {
	   a.setId(id);
		return po.save(a);
	}
	@DeleteMapping(value="/listspectateurs/{id}")
	public void  delete(@PathVariable(name="id") Long id) {
	    po.deleteById(id);
	}
	
	
}
